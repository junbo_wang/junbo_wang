{
    let endMath = 0;
    let canvas_02 = document.querySelector("#canvas_02");
    if (canvas_02.getContext) {
        let ctx = canvas_02.getContext("2d");
        let timer = setInterval(() => {
            // 先清空画板
            ctx.clearRect(0, 0, canvas_02.width, canvas_02.height);

            ctx.beginPath();
            ctx.arc(200, 200, 150, 0, 2 * Math.PI, false);
            ctx.stroke();
            /*画一个以（x,y）为圆心的以radius为半径的圆弧（圆），
            从startAngle开始到endAngle结束，
            按照anticlockwise给定的方向（默认为顺时针）来生成。*/
            ctx.closePath();

            // 0-2
            endMath = endMath > 2 ? 0 : endMath;

            ctx.beginPath();
            ctx.moveTo(200, 200);
            ctx.arc(200, 200, 100, 1.5 * Math.PI, endMath * Math.PI + 1.5 * Math.PI, false);
            ctx.fill();
            ctx.closePath();
            endMath += 0.008
        }, 1)
    }
}