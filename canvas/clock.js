{
    let canvas_03 = document.querySelector("#canvas_03");
    if (canvas_03.getContext) {
        let ctx = canvas_03.getContext("2d");
        let timer = () => {
            // 先清空画板
            ctx.clearRect(0, 0, canvas_03.width, canvas_03.height);

            // 画圆
            ctx.beginPath();
            ctx.strokeStyle = '#000000';
            ctx.arc(200, 200, 150, 0, 2 * Math.PI, false);
            ctx.stroke();
            ctx.closePath();

            // 绘制文本
            ctx.font = "20px serif";
            ctx.textAlign = "center";
            for (let i = 1; i <= 12; i++) {
                ctx.fillText(i, 120 * Math.sin(i / 6 * Math.PI) + 200, -120 * Math.cos(i / 6 * Math.PI) + 210)
            }

            let date = new Date();
            let s = date.getSeconds();
            let m = date.getMinutes() + s / 60;
            let h = date.getHours() + m / 60;

            // 时针hours
            ctx.beginPath();
            ctx.strokeStyle = '#000000';
            ctx.moveTo(200, 200);
            ctx.lineTo(60 * Math.sin(h / 6 * Math.PI) + 200, -60 * Math.cos(h / 6 * Math.PI) + 200);
            ctx.stroke();
            ctx.closePath();

            // 分针minutes
            ctx.beginPath();
            ctx.strokeStyle = '#000000';
            ctx.moveTo(200, 200);
            ctx.lineTo(80 * Math.sin(m / 30 * Math.PI) + 200, -80 * Math.cos(m / 30 * Math.PI) + 200);
            ctx.stroke();
            ctx.closePath();

            // 秒针seconds
            ctx.beginPath();
            ctx.strokeStyle = '#ff0000';
            ctx.moveTo(200, 200);
            ctx.lineTo(100 * Math.sin(s / 30 * Math.PI) + 200, -100 * Math.cos(s / 30 * Math.PI) + 200);
            ctx.stroke();
            ctx.closePath();
        };
        timer();
        setInterval(timer, 1000);
    }
}