{
    // 随机颜色值 - return string类型'#abc'
    function random_color() {
        let a = Math.floor(Math.random() * 16).toString(16);
        let b = Math.floor(Math.random() * 16).toString(16);
        let c = Math.floor(Math.random() * 16).toString(16);
        return "#" + a + b + c;
    }
    // 随机位置
    function random_seat(minNum, maxNum) {
        return Math.floor(Math.random()*(maxNum-minNum)+minNum)
    }
    function addReduce() {
        return Math.random()*20-10
    }

    // 画板
    let canvas_04 = document.querySelector("#canvas_04");
    if (canvas_04.getContext) {
        let ctx = canvas_04.getContext("2d");

        let a=200,b=200;
        let count = 0;
        let timer = setInterval(()=>{
            ctx.beginPath();
            ctx.moveTo(a,b);
            a = a+addReduce();
            b = b+addReduce();
            ctx.lineTo(a,b);
            ctx.stroke();
            ctx.closePath();
            if (a <= 0 || a >= 400 || b <= 0 || b >= 400) {
                a = random_seat(0,400);
                b = random_seat(0,400);
                count++;
            }
            if (count === 5) {
                clearInterval(timer)
            }
        },0)
    }
}