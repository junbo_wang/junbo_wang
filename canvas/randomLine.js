{
    let arr = [];
// 随机颜色值 - return string类型'#abc'
    function random_color() {
        let a = Math.floor(Math.random() * 16).toString(16);
        let b = Math.floor(Math.random() * 16).toString(16);
        let c = Math.floor(Math.random() * 16).toString(16);
        return "#" + a + b + c;
    }
// 重绘画板
    function paintCanvas_01(jsons, ctx) {
        for (let arr of jsons) {
            ctx.beginPath();
            ctx.moveTo(arr.x1, arr.y1);
            ctx.lineTo(arr.x2, arr.y2);
            let a = ctx.createLinearGradient(arr.x1, arr.y1, arr.x2, arr.y2);
            a.addColorStop(0, arr.color_0);
            a.addColorStop(1, arr.color_1);
            ctx.strokeStyle = a;
            ctx.stroke();
            ctx.closePath();
        }
    }
// 画板
    let canvas_01 = document.querySelector("#canvas_01");
    if (canvas_01.getContext) {
        let ctx = canvas_01.getContext("2d");

        let timer = setInterval(() => {
            // 开始坐标 & 结束坐标
            let x1 = Math.floor(Math.random() * 400);
            let y1 = Math.floor(Math.random() * 400);
            let x2 = Math.floor(Math.random() * 80) - 40 + x1;
            let y2 = Math.floor(Math.random() * 80) - 40 + y1;
            // 开始颜色 & 结束颜色
            let color_0 = random_color();
            // let color_1 = color_0;
            let color_1 = random_color();
            let json = {
                x1: x1,
                y1: y1,
                x2: x2,
                y2: y2,
                color_0: color_0,
                color_1: color_1,
            };
            arr.push(json);
            arr.length === 100 ? arr.splice(0, 1) : 0;
            // 先清空画板
            ctx.clearRect(0, 0, canvas_01.width, canvas_01.height);
            paintCanvas_01(arr, ctx);
        }, 0)
    }
}