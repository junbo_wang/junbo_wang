const col = 10; // 长度 y
const row = 20; // 高度 x
const square = 25; // 容器像素点边长
const SEED = [
	[// ::
		// 旋转
		[{ x: 0, y: 0 }, { x: 0, y: 1 }, { x: 1, y: 0 }, { x: 1, y: 1 }]
	],
	[ // ····
		[{ x: -1, y: 0 }, { x: 0, y: 0 }, { x: 1, y: 0 }, { x: 2, y: 0 }],
		[{ x: 0, y: -1 }, { x: 0, y: 0 }, { x: 0, y: 1 }, { x: 0, y: 2 }],
	],
	[ // :..
		[{ x: -1, y: 1 }, { x: -1, y: 0 }, { x: 0, y: 0 }, { x: 1, y: 0 }],
		[{ x: 0, y: 1 }, { x: -1, y: 1 }, { x: -1, y: 0 }, { x: -1, y: -1 }],
		[{ x: 0, y: 0 }, { x: 0, y: 1 }, { x: -1, y: 1 }, { x: -2, y: 1 }],
		[{ x: -1, y: 0 }, { x: 0, y: 0 }, { x: 0, y: 1 }, { x: 0, y: 2 }],
	],
	[ // ..:
		[{ x: 1, y: 1 }, { x: 1, y: 0 }, { x: 0, y: 0 }, { x: -1, y: 0 }],
		[{ x: 1, y: 0 }, { x: 0, y: 0 }, { x: 0, y: 1 }, { x: 0, y: 2 }],
		[{ x: 0, y: 0 }, { x: 0, y: 1 }, { x: 1, y: 1 }, { x: 1, y: 2 }],
		[{ x: 0, y: 1 }, { x: 1, y: 1 }, { x: 1, y: 0 }, { x: 1, y: -1 }],
	],
	[ // ·:.
		[{ x: -1, y: 1 }, { x: 0, y: 1 }, { x: 0, y: 0 }, { x: 1, y: 0 }],
		[{ x: -1, y: -1 }, { x: -1, y: 0 }, { x: 0, y: 0 }, { x: 0, y: 1 }],
	],
	[ // .:·
		[{ x: -1, y: 0 }, { x: 0, y: 0 }, { x: 0, y: 1 }, { x: 1, y: 1 }],
		[{ x: 0, y: -1 }, { x: -1, y: 0 }, { x: 0, y: 0 }, { x: -1, y: 1 }],
	],
	[ // .:.
		[{ x: -1, y: 0 }, { x: 0, y: 0 }, { x: 1, y: 0 }, { x: 0, y: 1 }],
		[{ x: 0, y: 1 }, { x: 0, y: 0 }, { x: 0, y: -1 }, { x: 1, y: 0 }],
		[{ x: -1, y: 0 }, { x: 0, y: 0 }, { x: 1, y: 0 }, { x: 0, y: -1 }],
		[{ x: 0, y: 1 }, { x: 0, y: 0 }, { x: 0, y: -1 }, { x: -1, y: 0 }],
	],
];

/**
 * 位置
 * @typedef {object} BlockType
 * @property {!number} x row
 * @property {!number} y col
 */
let block = {
	x: 0,
	y: 0
}

/**
 * 正在下落的方块
 * @typedef {object} DoingPosType
 * @property {!number} type
 * @property {BlockType} block
 * @property {!number} rotateNum
 */
let doingPos = {
	type: 1,
	block: block,
	roteteNum: 3
}

let donePos = [];

let speed = 1000; // 起始速度

/* 旋转 */
function rotate() {
	return move(0, 0, true);
}

function right() {
	return move(0, 1, false);
}

function left() {
	return move(0, -1, false);
}

function down() {
	return move(1, 0, false, true);
}


/**
 * 方块移动
 *
 * @param {number} x 行数
 * @param {number} y 列数
 * @param {boolean} rotate 是否旋转
 * @return {boolean} 移动成功 / 失败
 */
function move(x, y, rotate, doing2Done) {
	let newPos = deepClone(doingPos);
	newPos.block.x += x;
	newPos.block.y += y;
	if (rotate) {
		newPos.roteteNum += 1;
		if (newPos.roteteNum >= SEED[newPos.type].length) {
			newPos.roteteNum = 0;
		}
	}
	if (checkIsOut(newPos)) {
		if (doing2Done) {
			doingBlock2Done(doingPos);
		} else {
			doingPos = newPos;
		}
		return false;
	} else {
		doingPos = deepClone(newPos);
		return true;
	}
}

/**
 * 判断该位置是否可放
 *
 * @param {DoingPosType} checkPos 要检查方块
 * @return {boolean} 是 / 否
 */
function checkIsOut(checkPos) {
	let checkArr = doingPos2Arr(checkPos);
	// 判断是否超出页面
	for (let k = 0; k < checkArr.length; k++) {
		let checkEle = checkArr[k];
		if (checkEle.x >= row
			|| checkEle.y < 0 || checkEle.y >= col) {
			return true;
		}
	}
	// 判断是否与已落下方块重合
	for (let i = 0; i < donePos.length; i++) {
		let doneEle = donePos[i];
		for (let j = 0; j < checkArr.length; j++) {
			let checkEle = checkArr[j];
			if (doneEle.x === checkEle.x && doneEle.y === checkEle.y) {
				return true;
			}
		}
	}
	return false;
}

/**
 * 正在下落的方块转list
 *
 * @param {DoingPosType} doingPos
 * @return {BlockType[]}
 */
function doingPos2Arr(doingPos) {
	let dpClone = deepClone(doingPos);
	let arr = deepClone(SEED[dpClone.type][dpClone.roteteNum]);
	for (let i = 0; i < arr.length; i++) {
		// 	arr[i] = rotateFun(arr[i], dpClone.roteteNum);
		arr[i].x += dpClone.block.x;
		arr[i].y += dpClone.block.y;
	}
	return arr;
}

function doingBlock2Done(doingPos) {
	donePos.push(...doingPos2Arr(doingPos))
	init();
}

function init() {
	let type = random(SEED.length)
	doingPos = {
		type: type,
		block: { x: 0, y: parseInt(col / 2) },
		roteteNum: random(SEED[type].length)
	}
}

/**
 * 深拷贝
 *
 * @param {object} obj 需要拷贝的对象
 * @return {object} 拷贝后的对象
 */
function deepClone(obj) {
	// if (obj instanceof Array) {
	// 	return [...obj];
	// }
	return JSON.parse(JSON.stringify(obj));
}

/**
 * 随机数
 *
 * @param {!number} size 取值范围
 * @return {!number} 随机数(左闭右开)
 */
function random(size) {
	return parseInt(Math.random() * size);
}

/**
 * 旋转
 *
 * @Deprecated
 * @param {!BlockType} block 需要旋转的坐标点
 * @param {!number} count 旋转角度
 * @param {!BlockType} referBlock 基准点
 * @return {!BlockType} 旋转后坐标
 */
function rotateFun(block/*(0, 0)*/, count = 0, referBlock = { x: 0, y: -1 }) {
	// 平面上一点x1, y1, 绕平面上另一点x2, y2顺时针旋转θ角度 ，怎么求旋转后的x1, y1对应的坐标x，y
	// x = (x1 - x2)cosθ - (y1 - y2)sinθ + x2
	// y = (y1 - y2)cosθ + (x1 - x2)sinθ + y2
	let offsetX = block.x - referBlock.x;
	let offsetY = block.y - referBlock.y;
	const sin = Math.round(Math.sin(Math.PI / 2 * count));
	const cos = Math.round(Math.cos(Math.PI / 2 * count));
	let iNewX = offsetX * cos - offsetY * sin;
	let iNewY = offsetX * sin + offsetY * cos;
	if (iNewX === 0) {
		iNewX = 0;
	}
	if (iNewY === 0) {
		iNewY = 0;
	}
	return { x: iNewX, y: iNewY }; // (0, -1)
}
